package com.kafka.producer1.service;

import com.kafka.producer1.dto.request.PaymentStatusMessageRequestDTO;
import com.kafka.producer1.dto.response.PaymentStatusMessageResponseDTO;

import java.util.List;

public interface PaymentStatusMessagingService {
    void send(PaymentStatusMessageRequestDTO message);

    void send(String superPnr, String paymentId, String status);

    List<PaymentStatusMessageResponseDTO> sendRandom(int count);
}
