package com.kafka.producer1.controller;


import com.kafka.producer1.dto.request.PaymentStatusMessageRequestDTO;
import com.kafka.producer1.service.PaymentStatusMessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/producer")
public class ProducerController {

    @Autowired
    PaymentStatusMessagingService paymentStatusMessagingService;

    @PostMapping(
            value = "/m"
    )
    public ResponseEntity<?> sendMessage(@RequestBody PaymentStatusMessageRequestDTO requestDTO) {
        paymentStatusMessagingService.send(requestDTO);
        return ResponseEntity.ok("\"Message was Sent\"");
    }

    @PostMapping(
            value = "/m/r/{count}"
    )
    public ResponseEntity<?> sendRandomMessages(@PathVariable Integer count) {
        return ResponseEntity.ok(paymentStatusMessagingService.sendRandom(count));
    }
}
