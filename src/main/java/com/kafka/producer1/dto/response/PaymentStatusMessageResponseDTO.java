package com.kafka.producer1.dto.response;

import com.kafka.producer1.messaging.schema.PaymentStatusMessage;

public class PaymentStatusMessageResponseDTO {

    private String superPnr;
    private String paymentId;
    private String status;

    public String getSuperPnr() {
        return superPnr;
    }

    public void setSuperPnr(String superPnr) {
        this.superPnr = superPnr;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static PaymentStatusMessageResponseDTO build(PaymentStatusMessage message) {
        PaymentStatusMessageResponseDTO newMessage = new PaymentStatusMessageResponseDTO();
        newMessage.setPaymentId(message.getPaymentId().toString());
        newMessage.setSuperPnr(message.getSuperPnr().toString());
        newMessage.setStatus(message.getStatus().toString());
        return newMessage;
    }
}
