package com.kafka.producer1.messaging;

import com.kafka.producer1.dto.request.PaymentStatusMessageRequestDTO;
import com.kafka.producer1.dto.response.PaymentStatusMessageResponseDTO;
import com.kafka.producer1.messaging.schema.PaymentStatusMessage;
import com.kafka.producer1.service.PaymentStatusMessagingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@Service
public class PaymentStatusMessagingServiceImpl implements PaymentStatusMessagingService{

    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    @Qualifier("paymentStatusKafkaTemplate")
    KafkaTemplate<String, PaymentStatusMessage> kafkaTemplate;

    @Value("${kafka.topic}")
    String topic;

    String statusChoice[] = {"successful", "pending", "processing", "failed"};

    Random random = new Random();

    private void send(PaymentStatusMessage message) {
        kafkaTemplate.send(topic, message.getSuperPnr().toString(), message);
    }

    @Override
    public void send(PaymentStatusMessageRequestDTO message) {
        PaymentStatusMessage schemaMessage = new PaymentStatusMessage(message.getSuperPnr(),
                message.getPaymentId(), message.getStatus());
        send(schemaMessage);
    }

    @Override
    public void send(String superPnr, String paymentId, String status) {
        PaymentStatusMessage message = new PaymentStatusMessage(superPnr, paymentId, status);
        send(message);
    }

    @Override
    public List<PaymentStatusMessageResponseDTO> sendRandom(int count) {
        log.info("Sending {} message(s)", count);
        List<PaymentStatusMessageResponseDTO> messagesGenerated = new ArrayList<>();
        for (int i = 0 ; i < count; ++i) {
            String superPnr = randomAlphabetic(4);
            String paymentId = randomAlphabetic(6);
            String status = statusChoice[random.nextInt(3)];
            PaymentStatusMessage message = new PaymentStatusMessage(superPnr, paymentId, status);
            log.info("Sending message {} with content = {}", i+1, message);
            messagesGenerated.add(PaymentStatusMessageResponseDTO.build(message));
            send(message);
        }
        return messagesGenerated;
    }


}
